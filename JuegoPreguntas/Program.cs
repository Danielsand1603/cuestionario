﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JuegoPreguntas
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            Bienvenido con = new Bienvenido();
            con.FormClosed += Consulta_Closed;
            con.Show();

            Application.Run();
        }

        private static void Consulta_Closed(object sender, FormClosedEventArgs e)
        {
            ((Form)sender).FormClosed -= Consulta_Closed;

            if (Application.OpenForms.Count == 0)
            {
                Application.ExitThread();
            }
            else
            {
                Application.OpenForms[0].FormClosed += Consulta_Closed;
            }
        }

    }
}
