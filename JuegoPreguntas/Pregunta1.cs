﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JuegoPreguntas
{
    public partial class Pregunta1 : Form
    {

        int resultado = 0;
        int seleccion = 1;
        int error = 4;



        public Pregunta1()
        {
            InitializeComponent();
        }

        
       
        private void Form1_Load_1(object sender, EventArgs e)
        {
            Iniciar();
        }

        

        private void Iniciar()
        {
           
            this.pregunta.Text = "¿Cual de estos no es software de Virtualizacion? ?";
            this.opcion1.Text = "-Virtual Box";
            this.opcion2.Text = "-Virtual Iron.";
            this.opcion3.Text = "-VirtualLin Pro";
            this.opcion4.Text = "-Parallels Desktop";
            seleccion++;


        }

        private void pregunta2()
        {
            this.pregunta.Text = "Consiste en crear una representación basada en software, o virtual, de una entidad física como, por ejemplo, aplicaciones, servidores, redes y almacenamiento virtuales";
            this.opcion1.Text = "Software de Virtualizacion";
            this.opcion2.Text = "Aplicacion";
            this.opcion3.Text = "Sub-sistema operativo";
            this.opcion4.Text = "todas son correctas";
            seleccion++;
        }
        private void pregunta3()
        {
            this.pregunta.Text = "  Consiste en la administración y monitorización de una red de computadoras como una sola entidad de gestión desde una única consola de administrador basada en software ¿Es la definicion de Virtualización de servidores ? ";

            this.opcion1.Text = "Cierto";
            this.opcion2.Text = "Falso";

            seleccion++;
        }
        private void pregunta4()
        {
            this.pregunta.Text = "¿Orden de como funciona la virtualizacion de servidores?";
            this.opcion1.Text = "Adquisición del servidor, Implementación del servidor virtual,Creacion de maquinas virtuales,Instalacion de los sistemas,conexión SAN";
            this.opcion2.Text = "Creacion de maquinas virtuales,Instalacion de los sistemas,Adquisición del servidor,conexión SAN,Creacion de maquinas virtuales";
            this.opcion3.Text = "Instalacion de los sistemas,Adquisición del servidor,conexión SANCreacion de maquinas virtuales,Adquisición del servidor";
            this.opcion4.Text = "No existe el orden";
            seleccion++;
        }
        private void pregunta5()
        {
            this.pregunta.Text = "Ventajas de la Virtualización";     
            this.opcion1.Text = "Ahorro de costes,Aislamiento y seguridad,Clonación y migración de sistemas en caliente.";
            this.opcion2.Text = "Problemas de emulación de ciertos controladores,Muchos sistemas dependen de un solo equipo";
            this.opcion3.Text = "Rendimiento y Seguridad";
            this.opcion4.Text = "todas son correctas";
            seleccion++;
        }
        private void pregunta6()
        {
            this.pregunta.Text = "¿Virtualización de Hardware es? ,Es Cuando el SO completo corre de forma virtual sobre la máquina física";
            this.opcion1.Text = "VERDADERO";
            this.opcion2.Text = "FALSO";
            
            seleccion++;
        }
        private void pregunta7()
        {
            this.pregunta.Text = "¿Qué es Hipervisor?";
            this.opcion1.Text = "Es Cuando el SO completo corre de forma virtual sobre la máquina física";
            this.opcion2.Text = "Es una tecnica por la cual se obtiene varias máquinas lógicas que funcionan dentro de una máquina física";
            this.opcion3.Text = "Es cuando se cera un hardware sintetico el cual usan las maquinas virtuales como propios";
            this.opcion4.Text = "todas son correctas";
            seleccion++;
        }
        private void pregunta8()
        {
            this.pregunta.Text = "¿Que tipos de maquinas virtuales existen?";
            this.opcion1.Text = "De proceso";
            this.opcion2.Text = "De proceso y de sistema";
            this.opcion3.Text = "De sistema";
            this.opcion4.Text = "Ninguna de las anteriores";
            seleccion++;
        }
        private void pregunta9()
        {
            this.pregunta.Text = "¿Cuando es más util virtualizar un SO que instalarlo en un partición del PC?";
            this.opcion1.Text = "Cuando queremos cambiar de S.O. como si se tratase de cualquier otro programa";
            this.opcion2.Text = "Cuando queremos que el SO sea más potente";
            this.opcion3.Text = "Cuando queremos aumentar el nivel de abstracción del hardware externo de nuestro PC";
            this.opcion4.Text = "todas son correctas";
            seleccion++;
        }
        private void pregunta10()
        {
            this.pregunta.Text = "¿Que tres tipos de virtualizacion existen?";
            this.opcion1.Text = "virtualizacion completa del HW o nativa";
            this.opcion2.Text = "virtualizacion de emulacion del HW o no nativa";
            this.opcion3.Text = "virtualizacion a nivel de sistema operativo";
            this.opcion4.Text = "todas son correctas";
            seleccion++;
        }
        private void Limpiar()
        {
            this.opcion1.Checked = true;
        }

        private void Pregunta1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (seleccion)
            {

                case 1:
                    
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;
                        MessageBox.Show("Oportunidades "+error.ToString() , "Iniciemos");
                        if (error == 0)
                        {
                            MessageBox.Show("Tienes 3 oportunidades listo");
                            Application.Exit();
                        }
                    }

                    pregunta2();
                    Limpiar();
                   


                    break;

                    


                case 2:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta3();
                    Limpiar();
                    break;

                case 3:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta4();
                    Limpiar();
                    break;

                case 4:
                    if (this.opcion1.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta5();
                    Limpiar();
                    break;

                case 5:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta6();
                    Limpiar();
                    break;

                case 6:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta7();
                    Limpiar();
                    break;

                case 7:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta8();
                    Limpiar();
                    break;

                case 8:
                    if (this.opcion2.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta9();
                    Limpiar();
                    break;

                case 9:
                    if (this.opcion4.Checked == true)
                    {
                        resultado++;
                    }
                    else
                    {
                        error--;

                        MessageBox.Show("Oportunidades " + error.ToString(), "Incorrecto");

                        if (error == 0)
                        {
                            MessageBox.Show("Perdiste tus 3 oportunidades");
                            Application.Exit();
                        }

                    }
                    pregunta10();
                    Limpiar();
                    break;

                case 10:
                    if (this.opcion3.Checked == true)
                    {
                        resultado++;
                    }
                    if (resultado >= 7)
                    {
                        MessageBox.Show(resultado.ToString(), "FELICITACIONES", MessageBoxButtons.OK);
                        Application.Exit();
                    }


                    Iniciar();
                    Limpiar();
                    break;


                default:

                    break;
            }

        }

    }
}
