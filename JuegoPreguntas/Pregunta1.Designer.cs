﻿namespace JuegoPreguntas
{
    partial class Pregunta1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pregunta = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.opcion2 = new System.Windows.Forms.RadioButton();
            this.opcion1 = new System.Windows.Forms.RadioButton();
            this.opcion3 = new System.Windows.Forms.RadioButton();
            this.opcion4 = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pregunta
            // 
            this.pregunta.AutoSize = true;
            this.pregunta.BackColor = System.Drawing.Color.Transparent;
            this.pregunta.Font = new System.Drawing.Font("Arial", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pregunta.Location = new System.Drawing.Point(10, 80);
            this.pregunta.Margin = new System.Windows.Forms.Padding(10, 0, 4, 0);
            this.pregunta.Name = "pregunta";
            this.pregunta.Size = new System.Drawing.Size(0, 19);
            this.pregunta.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(348, 22);
            this.label3.TabIndex = 17;
            this.label3.Text = "Selecciona la opción que creas correcta";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(898, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(382, 28);
            this.label1.TabIndex = 15;
            this.label1.Text = "Preguntas de seleccion multiple";
            // 
            // opcion2
            // 
            this.opcion2.AutoSize = true;
            this.opcion2.BackColor = System.Drawing.Color.Transparent;
            this.opcion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opcion2.Location = new System.Drawing.Point(36, 191);
            this.opcion2.Margin = new System.Windows.Forms.Padding(4);
            this.opcion2.Name = "opcion2";
            this.opcion2.Size = new System.Drawing.Size(14, 13);
            this.opcion2.TabIndex = 12;
            this.opcion2.TabStop = true;
            this.opcion2.UseVisualStyleBackColor = false;
            // 
            // opcion1
            // 
            this.opcion1.AutoSize = true;
            this.opcion1.BackColor = System.Drawing.Color.Transparent;
            this.opcion1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opcion1.Location = new System.Drawing.Point(36, 135);
            this.opcion1.Margin = new System.Windows.Forms.Padding(4);
            this.opcion1.Name = "opcion1";
            this.opcion1.Size = new System.Drawing.Size(14, 13);
            this.opcion1.TabIndex = 11;
            this.opcion1.TabStop = true;
            this.opcion1.UseVisualStyleBackColor = false;
            // 
            // opcion3
            // 
            this.opcion3.AutoSize = true;
            this.opcion3.BackColor = System.Drawing.Color.Transparent;
            this.opcion3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opcion3.Location = new System.Drawing.Point(36, 243);
            this.opcion3.Margin = new System.Windows.Forms.Padding(4);
            this.opcion3.Name = "opcion3";
            this.opcion3.Size = new System.Drawing.Size(14, 13);
            this.opcion3.TabIndex = 13;
            this.opcion3.TabStop = true;
            this.opcion3.UseVisualStyleBackColor = false;
            // 
            // opcion4
            // 
            this.opcion4.AutoSize = true;
            this.opcion4.BackColor = System.Drawing.Color.Transparent;
            this.opcion4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.opcion4.Location = new System.Drawing.Point(36, 298);
            this.opcion4.Margin = new System.Windows.Forms.Padding(4);
            this.opcion4.Name = "opcion4";
            this.opcion4.Size = new System.Drawing.Size(14, 13);
            this.opcion4.TabIndex = 14;
            this.opcion4.TabStop = true;
            this.opcion4.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1045, 352);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(196, 40);
            this.button1.TabIndex = 10;
            this.button1.Text = "Siguiente";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Pregunta1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::JuegoPreguntas.Properties.Resources.fondo_pared_hormigon_habitacion_oscura_pared_piso_interiores_textura_concreta_49683_2409;
            this.ClientSize = new System.Drawing.Size(1293, 415);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pregunta);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opcion4);
            this.Controls.Add(this.opcion3);
            this.Controls.Add(this.opcion2);
            this.Controls.Add(this.opcion1);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Pregunta1";
            this.Text = "Pregunta1";
            this.Load += new System.EventHandler(this.Pregunta1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label pregunta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton opcion2;
        private System.Windows.Forms.RadioButton opcion1;
        private System.Windows.Forms.RadioButton opcion3;
        private System.Windows.Forms.RadioButton opcion4;
        private System.Windows.Forms.Button button1;
    }
}